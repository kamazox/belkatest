package ru.belkacar;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ForkJoinPool;
import java.util.function.Consumer;

import static org.junit.Assert.assertEquals;

public class ParallelProcessorImplTest {
    @Test
    public void test() {
        testWithPool(ForkJoinPool.commonPool());
    }

    @Test
    public void testCustomPool() {
        testWithPool(Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors()));
    }

    private void testWithPool(ExecutorService executorService) {
        List<String> list = new LinkedList<>();
        StringBuilder sb = new StringBuilder();
        int expected = 0;
        for (int i = 0; i < 100; i++) {
            list.add(sb.toString());
            expected += sb.toString().length();
            sb.append("a");
        }
        ParallelProcessor<String, Integer> pp = new ParallelProcessorImpl<>(executorService);
        List<Integer> results = new LinkedList<>();
        Consumer<Pair<String, Integer>> rc = r -> results.add(r.getB());
        int res = pp.process(list, s -> s.length(), rc, Integer::sum);
        List<Integer> cmp = new LinkedList<>(results);
        Collections.sort(cmp);
        assertEquals(cmp, results);
        assertEquals(expected, res);
    }

    @Test
    public void testEmpty() {
        new ParallelProcessorImpl<String, Integer>()
                .process(new ArrayList<>(), s -> s.length(), r -> {}, Integer::sum);
    }

    @Test(expected = NullPointerException.class)
    public void testNull() {
        new ParallelProcessorImpl<String, Integer>()
                .process(null, null, null, null);
    }
}
