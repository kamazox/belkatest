package ru.belkacar;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.function.Consumer;

public class Main {
    public static void main(String[] args) {
        if (args.length < 2) {
            throw new RuntimeException("You should specify input and output file names.");
        }
        String in = args[0];
        String out = args[1];
        try {
            processFile(in, out);
        } catch (IOException e) {
            throw new UncheckedIOException(e.getMessage(), e);
        }
    }

    private static void processFile(String in, String out) throws IOException {
        wrapCloseable(new IterableFileStrings(in), input -> {
            try {
                wrapCloseable(new LinesToFileConsumer(out), rc -> {
                    ParallelProcessor<String, Integer> processor = new ParallelProcessorImpl<>();
                    LinesToFileConsumer lfc = (LinesToFileConsumer) rc;
                    Integer res = processor.process((IterableFileStrings) input,
                            s -> s.length(),
                            e -> lfc.accept(e.getA() + " " + e.getB() + '\n'),
                            Integer::sum
                    );
                    if (res != null) {
                        lfc.accept(String.valueOf(res));
                    }
                });
            } catch (IOException e) {
                throw new UncheckedIOException(e);
            }
        });
    }

    private static void wrapCloseable(AutoCloseable closeable, Consumer<AutoCloseable> consumer) {
        try {
            consumer.accept(closeable);
        } finally {
            try {
                closeable.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
