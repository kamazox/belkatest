package ru.belkacar;

import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.Function;

public interface ParallelProcessor<T, R> {
    R process(Iterable<T> source,
              Function<T, R> function,
              Consumer<Pair<T, R>> resultCallback,
              BinaryOperator<R> reducer
    );
}
