package ru.belkacar;

import java.io.*;
import java.util.Iterator;

public class IterableFileStrings implements Iterable<String>, AutoCloseable {
    private final BufferedReader reader;

    public IterableFileStrings(String fileName) throws FileNotFoundException {
        reader = new BufferedReader(new FileReader(fileName));
    }

    private class FileStringsIterator implements Iterator<String> {
        private boolean gotNext = false;
        private String next = null;

        private void getNext() {
            if (!gotNext) {
                try {
                    next = reader.readLine();
                } catch (IOException e) {
                    throw new UncheckedIOException(e);
                }
                gotNext = true;
            }
        }

        @Override
        public boolean hasNext() {
            getNext();
            return next != null;
        }

        @Override
        public String next() {
            getNext();
            gotNext = false;
            return next;
        }
    }

    @Override
    public Iterator<String> iterator() {
        return new FileStringsIterator();
    }

    @Override
    public void close() throws Exception {
        reader.close();
    }
}
