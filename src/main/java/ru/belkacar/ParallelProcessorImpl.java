package ru.belkacar;

import java.util.*;
import java.util.concurrent.*;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.Function;

import static java.util.Objects.requireNonNull;

public class ParallelProcessorImpl<T, R> implements ParallelProcessor<T, R> {
    private final ExecutorService executor;

    public ParallelProcessorImpl() {
        executor = ForkJoinPool.commonPool();
    }

    public ParallelProcessorImpl(ExecutorService executor) {
        this.executor = executor;
    }

    @Override
    public R process(Iterable<T> source,
                     Function<T, R> function,
                     Consumer<Pair<T, R>> resultCallback,
                     BinaryOperator<R> reducer
    ) {
        requireNonNull(source, "Cannot process null.");
        requireNonNull(function, "Processing function cannot be null.");
        requireNonNull(resultCallback, "Result consumer cannot be null.");
        final int processorCount = Runtime.getRuntime().availableProcessors();
        final List<T> list = new ArrayList<>(processorCount);
        final Map<T, Pair<T, R>> resultMap = new HashMap<>(processorCount);
        final List<Future<Pair<T, R>>> futures = new ArrayList<>(processorCount);
        R reduced = null;
        Iterator<T> iterator = source.iterator();
        while (iterator.hasNext()) {
            list.clear();
            resultMap.clear();
            futures.clear();
            int i = 0;
            while (iterator.hasNext() && i < processorCount) {
                T data = iterator.next();
                list.add(data);
                futures.add(executor.submit(() -> new Pair<>(data, function.apply(data))));
                i++;
            }
            for (int j = 0; j < i; j++) {
                Pair<T, R> p = silentGet(futures.get(j));
                resultMap.put(p.getA(), p);
            }
            for (T t : list) {
                Pair<T, R> p = resultMap.get(t);
                resultCallback.accept(p);
                if (reducer != null) {
                    reduced = (reduced == null) ? p.getB() : reducer.apply(reduced, p.getB());
                }
            }
        }
        return reduced;
    }

    private <R> R silentGet(Future<R> future) {
        try {
            return future.get();
        } catch (InterruptedException e) {
            throw new RuntimeException("Parallel processing was interrupted", e);
        } catch (ExecutionException e) {
            throw new RuntimeException(e.getMessage(), e.getCause());
        }
    }
}
