package ru.belkacar;

import java.io.FileWriter;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.function.Consumer;

public class LinesToFileConsumer implements Consumer<String>, AutoCloseable {
    private final FileWriter writer;

    public LinesToFileConsumer(String fileName) throws IOException {
        writer = new FileWriter(fileName);
    }

    @Override
    public void accept(String s) {
        try {
            writer.write(s);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    @Override
    public void close() throws Exception {
        writer.close();
    }
}
